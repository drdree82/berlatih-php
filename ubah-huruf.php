<?php
$string = "abcdefghijklmnopqrstuvwxyz";
$shift = 1;
$shiftedString = "";

function ubah_huruf($string){
//kode di sini
 for ($i = 0; $i < strlen($string); $i++)
{
    $ascii = ord($string[$i]);
    $shiftedChar = chr($ascii + $shift);

    $shiftedString .= $shiftedChar;
}

echo $shiftedString;

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu
?>